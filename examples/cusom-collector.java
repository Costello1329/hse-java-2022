import java.util.Set;
import java.util.TreeSet;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;

record Student(String name, int age) {}

class AverageAgeAccumulator {
    AverageAgeAccumulator(int studentsCount, int ageSum) {
        this.studentsCount = studentsCount;
        this.ageSum = ageSum;
    }

    int studentsCount;
    int ageSum;
}

class AverageAgeCollector implements Collector<Student, AverageAgeAccumulator, Double> {
    @Override
    public Supplier<AverageAgeAccumulator> supplier() {
        return () -> new AverageAgeAccumulator(0, 0);
    }

    @Override
    public BiConsumer<AverageAgeAccumulator, Student> accumulator() {
        return (acc, student) -> {
            ++ acc.studentsCount;
            acc.ageSum += student.age();
        };
    }

    @Override
    public BinaryOperator<AverageAgeAccumulator> combiner() {
        return (acc1, acc2) -> new AverageAgeAccumulator(
            acc1.studentsCount + acc2.studentsCount,
            acc1.ageSum + acc2.ageSum
        );
    }

    @Override
    public Function<AverageAgeAccumulator, Double> finisher() {
        return (acc) -> (double)acc.ageSum / acc.studentsCount;
    }

    @Override
    public Set<Characteristics> characteristics() {
        final Set<Characteristics> s = new TreeSet<>();
        s.add(Characteristics.CONCURRENT);
        s.add(Characteristics.UNORDERED);
        return s;
    }
}

public class Main {
    public static void main(String[] args) {
        double avgAge = Stream.of(
            new Student("Petr", 21),
            new Student("Ivan", 22),
            new Student("Mikhail", 19)
        ).collect(new AverageAgeCollector());

        System.out.println(avgAge);
    }
}
