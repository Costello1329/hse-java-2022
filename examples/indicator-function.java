import java.util.function.Function;
import java.util.function.Predicate;

public class Main {
    public static void main (String[] args) {
        final Predicate<Double> inSegment = x -> x >= 3. && x <= 6.;
        final Function<Double, Double> result = indicator(inSegment);
        System.out.println(result.apply(5.));
        System.out.println(result.apply(2.));
    }

    public static <T> Function<T, Double> indicator (final Predicate<T> predicate) {
        return (final T x) -> predicate.test(x) ? 1. : 0.;
    }
}
