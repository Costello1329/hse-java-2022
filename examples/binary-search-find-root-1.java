public class Main {
    public static double f (final double x) {
        return Math.log(Math.log(1 / x));
    }

    public static double findRoot (double l, double r, final double precision) {
        final int iterationsAmount = (int)Math.ceil(Math.log((r - l) / precision) / Math.log(2));

        for (int i = 0; i < iterationsAmount; i ++) {
            final double m = (r + l) / 2.;

            if (f(m) * f(l) > 0.)
                l = m;
            else
                r = m;
        }

        return (r + l) / 2;
    }

    public static void main(String[] args) {
        final double precision = 1e-6;
        System.out.println("Got: " + findRoot(.1, 1., precision));
        System.out.println("Expected: " + 1 / Math.E);
    }
}
