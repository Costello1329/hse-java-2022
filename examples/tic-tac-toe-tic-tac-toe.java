import java.util.ArrayList;
import java.util.Optional;

public class TicTacToe {
    public TicTacToe () {
        _status = Status.Active;
        _currentPlayer = Player.Cross;
        _field = new ArrayList<Optional<Player>>();

        for (int i = 0; i < 9; i ++)
            _field.add(Optional.empty());
    }

    public Player currentPlayer () { return _currentPlayer; }
    public Status status () { return _status; }

    public Optional<Player> figureAt (final int cell) {
        if (cell < 0 || cell >= 9)
            return Optional.empty();

        return _field.get(cell);
    }

    public boolean makeMove (final int cell) {
        if (_status != Status.Active || cell < 0 || cell >= 9 || _field.get(cell).isPresent())
            return false;

        _field.set(cell, Optional.of(_currentPlayer));
        _updateStatus();

        if (_status == Status.Active)
            _currentPlayer = _currentPlayer == Player.Circle ? Player.Cross : Player.Circle;
        return true;
    }


    private void _updateStatus () {
        if (_status != Status.Active)
            return;

        for (final Player type : Player.values())
            if (
                (checkCell(0, type) && checkCell(1, type) && checkCell(2, type)) ||
                (checkCell(3, type) && checkCell(4, type) && checkCell(5, type)) ||
                (checkCell(6, type) && checkCell(7, type) && checkCell(8, type)) ||
                (checkCell(0, type) && checkCell(3, type) && checkCell(6, type)) ||
                (checkCell(1, type) && checkCell(4, type) && checkCell(7, type)) ||
                (checkCell(2, type) && checkCell(5, type) && checkCell(8, type)) ||
                (checkCell(0, type) && checkCell(4, type) && checkCell(8, type)) ||
                (checkCell(2, type) && checkCell(4, type) && checkCell(6, type))
            ) {
                _status = Status.NonDraw;
                break;
            }

        if (_status != Status.Active)
            return;

        for (int cell = 0; cell < 9; cell ++)
            if (_field.get(cell).isEmpty())
                return;

        _status = Status.Draw;
    }

    private boolean checkCell (final int cell, final Player type) {
        return _field.get(cell).isPresent() && _field.get(cell).get() == type;
    }

    private Status _status;
    private Player _currentPlayer;
    private final ArrayList<Optional<Player>> _field;
}
