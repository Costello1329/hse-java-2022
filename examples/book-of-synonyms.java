import java.util.*;

public class Main {
    public static void main (String[] args) {
        /**
         * КНИГА СИНОНИМОВ
         *
         * Условие:
         * Необходимо реализовать книгу синонимов: книгу, которая запоминает все похожие слова и группирует их в одно
         * множество. Необходимо уметь добавлять новые пары синонимов и обновлять книгу, а также предоставлять
         * любую группу синонимов в любой момент времени.
         *
         * На вход подается последовательсть из произвольного количества команд. Всего существует три типа комманд:
         * add {firstWord} {secondWord}
         * get {word}
         * exit
         *
         * add: добавляет пару новых слов в книгу синонимов. Необходимо перебрать все 4 варианта присутствия слов в
         * книге. В случае, если оба слова присутствуют, группы синонимов надо слить в одну. В случае, если присутствует
         * только одно слово, второе нужно добавить к нему в группу. В случае, если ни одного слова нет в книге, нужно
         * создать новую группу синонимов и добавить в нее два новых слова
         *
         * get: выводит группу синонимов, содержащую переданное слово в отсортированном порядке
         *
         * exit: останавливает программу
         */

        final Map<String, Set<String>> book = new HashMap<>();
        final Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {
            final String operation = scanner.next();

            if (operation.equals("add")) {
                final String first = scanner.next();
                final String second = scanner.next();

                if (book.containsKey(first) && !book.containsKey(second)) {
                    book.get(first).add(second);
                    book.put(second, book.get(first));
                }

                else if (!book.containsKey(first) && book.containsKey(second)) {
                    book.get(second).add(first);
                    book.put(first, book.get(second));
                }

                else if (!book.containsKey(first) && !book.containsKey(second)) {
                    final Set<String> newSet = new TreeSet<>();
                    newSet.add(first);
                    newSet.add(second);
                    book.put(first, newSet);
                    book.put(second, newSet);
                }

                else {
                    final Set<String> firstSet = book.get(first);
                    final Set<String> secondSet = book.get(second);

                    for (final String word : firstSet)
                        book.put(word, secondSet);

                    secondSet.addAll(firstSet);
                }
            }

            else if (operation.equals("get")) {
                final String word = scanner.next();

                if (book.containsKey(word))
                    System.out.println(book.get(word));

                else
                    System.out.println("No such word");
            }

            else
                break;
        }
    }
}
