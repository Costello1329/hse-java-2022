import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final double a = scanner.nextDouble();
        final double b = scanner.nextDouble();
        final double c = scanner.nextDouble();
        final double d = scanner.nextDouble();
        doIntersect(a, b, c, d);
    }

    public static void doIntersect(
        final double a,
        final double b,
        final double c,
        final double d
    ) {
        if (!cmpDouble(a, c)) {
            System.out.println("1 точка пересечения");
        } else if (cmpDouble(b, d)) {
            System.out.println("совпадают");
        } else {
            System.out.println("Не пересекаются");
        }
    }

    public static boolean cmpDouble(final double first, final double second) {
        final double eps = 1e-6;
        return Math.abs(first - second) < eps;
    }
}
