1^2 + 2^2 + ... + n^2 = n * (n + 1) * (2n + 1) / 6


int sum_square(int n) {
	int ans = 0;

	for (int i = 1; i <= n; i += 1) {
		ans += i * i;
	}

	return ans;
}


void main() {
	int n = read_number();
	int res = sum_square(n);
	print(res);
}
