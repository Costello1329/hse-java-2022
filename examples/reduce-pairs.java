import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

record Point (double x, double y) {
    public static double distance (final Point first, final Point second) {
        return Math.sqrt(Math.pow(first.x - second.x, 2.) + Math.pow(first.y - second.y, 2.));
    }
}

record Path (Point start, Point end, double length) {
    public static Path empty () { return new Path(null, null, 0.); }
    public boolean isEmpty () { return start == null || end == null; }
}

public class Main {
    public static void main (String[] args) {
        System.out.println(new BufferedReader(new InputStreamReader(System.in))
            .lines()
            .map(line -> Arrays.stream(line.split(" ")).limit(2).map(Double::parseDouble).collect(Collectors.toList()))
            .map(coords -> new Point(coords.get(0), coords.get(1)))
            .reduce(
                Path.empty(),
                (final Path path, final Point point) ->
                    path.isEmpty() ?
                    new Path(point, point, 0.) :
                    new Path(path.start(), point, path.length() + Point.distance(path.end(), point)),
                (final Path first, final Path second) -> {
                    if (first.isEmpty() && second.isEmpty())
                        return Path.empty();
                    else if (first.isEmpty())
                        return second;
                    else if (second.isEmpty())
                        return first;
                    else
                        return new Path(
                            first.start(),
                            second.end(),
                            first.length() + second.length() + Point.distance(first.end(), second.start())
                        );
                }
            ).length());
    }
}
