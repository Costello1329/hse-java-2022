a1 a2 a3 a4 a5 a6 a7 ...
a1 a3 a5 ... a2 a4 a6 ...
int[] -> int[]


int[] reorder(int n, int[] arr) {
	int[n] ans;

	for (int i = 0; i < n; i += 2) {
		ans[i / 2] = arr[i];
	}

	for (int i = 1; i < n; i += 2) {
		ans[(n + 1) / 2 + i / 2] = arr[i];
	}

	return ans;
}

void main() {
	int n = read_number()
	int[n] arr;

	for (int i = 0; i < n; i += 1) {
		arr[i] = read_number();
	}

	int[n] res = reorder(n, arr);
	print(res);
}
