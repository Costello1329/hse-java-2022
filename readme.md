# Введение в программирование на языке Java


## Команда курса
+ [Константин Леладзе](https://kleladze.ru): Организатор, лектор, семинарист
+ Марьяна Рубаненко: семинарист
+ Кирилл Семенников: семинарист
+ Илья Жорниченко: семинарист

## Ссылки
+ Информация о курсе:
  + Подробно расписана в этом документе в разделе [**информация о курсе**](#Информация-о-курсе)
  + Также, можно прочесть сам [ПУД](https://dp.hse.ru/#/summary?implementationId=4509553493)
+ [Рабочее пространство в Slack](https://join.slack.com/t/hse-cs-cources/shared_invite/zt-1f9qw0yrr-Jo0PpzLZCGdqHPOnLdbExA)
+ [Программа курса](/roadmap.md): список тем и разбиение их по блокам программы. **Внимание: не стоит путать блок программы с блоком контеста.**
+ [План занятий](/lessons.md)
+ Материалы лекций:
  + Записи занятий:
    + [Лекция 1](https://events.webinar.ru/gsbhse/714977084/record-new/1804621147)
    + [Лекция 2](https://events.webinar.ru/gsbhse/714977084/record-new/621635710)
    + [Лекция 3](https://events.webinar.ru/gsbhse/714977084/record-new/656537747)
    + [Лекция 4](https://events.webinar.ru/gsbhse/714977084/record-new/1664798342)
    + [Лекция 5](https://events.webinar.ru/gsbhse/714977084/record-new/1957586377)
    + [Лекция 6](https://events.webinar.ru/gsbhse/714977084/record-new/674304425)
    + [Лекция 7](https://events.webinar.ru/gsbhse/1465422069/record-new/1837687032)
    + [Лекция 8](https://events.webinar.ru/gsbhse/1465422069/record-new/2063993094)
    + [Лекция 9](https://youtu.be/xglP8mATFAE)
    + Лекция 10: [часть 1](https://events.webinar.ru/gsbhse/1465422069/record-new/1594446051), [часть 2](https://youtu.be/65lwgwfrjdg)
    + [Лекция 11](https://events.webinar.ru/gsbhse/1465422069/record-new/1330081703)
    + [Лекция 12](https://events.webinar.ru/gsbhse/1465422069/record-new/1615355444)
    + [Лекция 13](https://events.webinar.ru/gsbhse/2033915450/record-new/1409882093)
    + [Лекция 14](https://events.webinar.ru/gsbhse/2033915450/record-new/292870153)
    + [Лекция 15](https://events.webinar.ru/gsbhse/2033915450/record-new/979170402)
    + [Лекция 16](https://events.webinar.ru/gsbhse/2033915450/record-new/1083748294)
    + [Лекция 17](https://youtu.be/Q9VK-TSPvfM)
    + [Лекция 18](https://youtu.be/lGJR_9yGBEA)
  + [Презентации](/lectures)
+ Контест:
  + [Ссылка на гугл-форму с регистрацией](https://forms.gle/QNVjtKpFeHRjADHy6). Её необходимо пройти, иначе мы не сможем выставить вам оценку за контест, что неминуемо приведёт к пересдаче. **Внимание:** оценка будет выставлена вам именно за те решения, которые были отправленны с того аккаунта, Stepik ID которого вы указали при заполнении формы. **Определение:** аккаунтом студента является тот аккаунт, который он указал при заполненнии формы. Никакой другой аккаунт не может являться аккаунтом этого студента. Другими словами, двух аккаунтов у одного студента быть не может.
  + [Ссылка](https://stepik.org/course/130471)
+ [Инструкция по работе с контестом](/contest-guide.md)
+ [Сводная таблица по курсу](https://docs.google.com/spreadsheets/d/19dRwJFimQ66A9xYbPFa-B7fp8IPg_7AhpIrybP__vlM/edit?usp=sharing)


## Информация о курсе
### Цель курса
Помочь освоить базовые принципы, понятия и навыки программирования, познакомить слушателей курса с языком программирования Java.

### Результаты прохождения курса
+ Знание базовых понятий программирования
+ Умение придумывать решение часто встречающихся практических задач в процедурном, объектно–ориентированном и функциональном стилях.
+ Понимание основного синтаксиса Java, широко востребованного в индустрии кросплатформенного языка программирования.
+ Умение использовать язык программирования Java для решения практических задач

### План курса
Курс состоит из нескольких временных перидов:
+ Основной период:
  + Лектор и семинаристы проводят занятия; студенты решают задачи в [первых трех блоках контеста](/readme.md#блоки-контеста) с домашними заданиями, как только он открывается.
  + Промежуток: `07.11.22 – 28.05.23`
+ Предэкзаменационный период:
  + Ассистент проверяет решения задач, отправленные на [рецензирование](/contest-guide.md#рецензирование); студенты, допущенные до бонусного блока контеста, решают [бонусный блок контеста](/readme.md#бонусный-блок); произвотся проверка решений на плагиат; организатор курса переносит оценки из контеста в сводную таблицу.
  + Промежуток: `29.05.23 – 25.06.23`
+ Экзаменационный период:
  + Студенты [сдают экзамен](/readme.md#экзамен); начинается [закрытие долгов по контесту](/readme.md#закрытие-долгов-по-контесту): студенты решают блок контеста для должников.
  + Промежуток: `26.06.23 – 30.06.23`
+ Послеэкзаменационный период:
  + Студенты сдают [пересдачу](/readme.md#пересдача).

### Формат занятий
+ Занятия проходят онлайн, с использованием _Zoom_, _Яндекс Телемост_ и _Webinar.ru_.
+ Лекционный материал включает в себя демонстрацию синтаксиса языка Java, обзор ключевых особенностей языка и описание основных компонент языка Java. Семинарские занятия посвящены демонстрации решений часто встречающих задач с помощью Java и взаимодействию с аудиторией. Большое внимание уделяется практическим заданиям и самостоятельной работе студентов.
+ Лекционное занятие проходит синхронно у всех групп раз в неделю. Семинарские занятия – для каждой группы по-отдельности, с той же периодичнотсью.

### Контест
Представляет из себя курс на Stepik. Контест с заданиями разделен на несколько блоков. Каждый блок содержит в себе несколько задач. У каждого задания в контесте есть определенная **стоимость**. При успешном выполнении задания, то есть при прохождении всех автотестов и, далее, рецензирования (если оно присутствует для этого задания), студент получает количество баллов, равное стоимости задачи, либо, в случае задания с [рецензированием](/contest-guide.md#рецензирование), количество баллов, установленное проверяющим. У каждого блока в контесте есть собственный **дедлайн**. Задания, на сдачу которых претендует студент, должны быть отправлены в контест и пройти автотесты строго до наступления дедлайна. Перед приступлением к решению задач, необходимо прочитать [инструкцию по работе с контестом](/contest-guide.md).

### Разделение по модулям
В каждом модуле планируеся по шесть лекционных и семинарских занятий для каждой группы.

#### Блок 1
  + Открыт: `05.12.2022 00:00` <!-- Понедельник после 4-ой лекции -->
  + Дедлайн: `08.01.2023 23:59` <!-- Воскресенье недели перед 7-ой лекцией -->
  + Рецензирование: Нет
  + Стоимость: `50`
  + Пороговый балл для допуска к бонусному блоку: `25`
#### Блок 2
  + Открыт: `30.01.2023 00:00` <!-- Понедельник после 8-ой лекции -->
  + Дедлайн: `09.04.2023 23:59` <!-- Воскресенье недели перед 14-ой лекцией -->
  + Рецензирование:
    + Задача `2.1.3: BigInt`
    + Задача `2.2.3: Geometry`
  + Стоимость: `75`
  + Пороговый балл для допуска к бонусному блоку: `30`
#### Блок 3
  + Открыт: `10.04.2023 00:00` <!-- Понедельник после 13-ой лекции -->
  + Дедлайн: `28.05.2023 23:59` <!-- Воскресенье недели c 18-ой лекцией -->
  + Рецензирование:
    + Задача `3.4.2: Min & max`
    + Задача `3.4.3: Ten most frequent words`
  + Стоимость: `50`
  + Пороговый балл для допуска к бонусному блоку: `20`
#### Бонусный блок
Для желающих получить дополнительное количество баллов будет создан специальный блок, получить баллы за который можно будет только при условии наличия допуска к нему.
  + Открыт: `29.05.2023 14:40` <!-- ~ Через неделю после 18-ой лекции -->
  + Длительность: `3 часа` <!-- Добавить пол часа на организационные моменты -->
  + Рецензирование: Нет
  + Стоимость: `25`
  + Допуск: до этого блока допускаются все желающие, однако **получат оценку за него лишь те студенты, которые набрали пороговые баллы для допуска к бонусному блоку за каждый из трех первых блоков контеста**.
#### Блок для должников
Блок, который необходимо сдать на полный балл всем, кто не набрал суммарно `50` баллов за первые три блока контеста для получения оценки.
  + Открыт: `29.05.2023 00:00` <!-- ~ Две недели до экзамена, в первой половине Июня -->
  + Дедлайн: `нет`
  + Рецензирование: Нет
  + Стоимость: `50`
  + **Внимание**: баллы, полученные за этот блок не суммируются с баллами, полученными за другие блоки. Вместо этого, за него можно получить лишь то количество баллов, которое недостает до получения `50` баллов за контест с домашними заданиями.
  + _Например, если у вас за контест в сумме `41` балл, то блок для должников вам все равно нужно сдать на `50` баллов, однако получите за него вы лишь `50 - 41 = 9` баллов._
#### Экзаменационный блок
Блок, содержащий экзаменационные вопросы и задачи.
  + Открыт: `26.06.2023 13:00` <!-- ~ Желательно под конец сессии, то есть во второй половине Июня -->
  + Длительность: `2 часа`
  + Рецензирование: нет
  + Стоимость: `50`

### Плагиат
Естественно, списывание решений запрещено, каждая задача должна быть решена студентом самостоятельно. Если при проверке решений будут найдены два очень похожих друг на друга решения, **отправленных с двух разных аккаунтов**, то оценки за оба решения будут обнулены. Проверка решений на списывание производится с использованием автоматизированной программы, находящей схожие решения, определяющей допустимый порог заимствования кода.

### Пересдача
Пересдача проходит в том же формате, только в [другой период](/readme.md#план-курса). Критерии оценивания – аналогичные.

### Система оценивания
В ходе курса вы можете получить `250 первичных баллов`:
+ `50` – за первый блок контеста
+ `75` – за второй блок контеста
+ `50` – за третий блок контеста
+ `25` – за бонусный блок контеста
+ `50` – за экзаменационный блок контеста

Финальная оценка вычисляется по формуле `round(primary_points, / 25)`.

**Внимание: Для успешного прохождения курса, студент должен получить как минимум `50` баллов суммарно за первые три блока контеста, либо решить блок контеста для должников на полный балл.**


## Copyright

![Creative Commons Licence](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

Все материалы доступны по лицензии [Creative Commons «Attribution-ShareAlike» 4.0](http://creativecommons.org/licenses/by-sa/4.0/). \
При заимствовании любых материалов из данного репозитория, необходимо оставить ссылку на него, а также, указать мое имя: **Константин Леладзе**.

__© Konstantin Leladze__
