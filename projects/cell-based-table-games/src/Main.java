import core.*;
import tafl.TaflEngine;
import tafl.TaflMove;
import tafl.engines.ardri.ArdRiEngine;
import tafl.engines.brandubh.BrandubhEngine;
import tafl.engines.tablut.TablutEngine;
import tictactoe.TicTacToeEngine;
import tictactoe.TicTacToeMove;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);

        final AbstractEngine<
            ? extends AbstractFigure,
            ? extends AbstractField<? extends AbstractFigure>,
            ? extends Move
        > engine;

        while (true) {
            System.out.println("Choose game:");
            System.out.println("1. Ard Ri");
            System.out.println("2. Brandubh");
            System.out.println("3. Tablut");
            System.out.println("4. Tic Tac Toe");

            final int game = scanner.nextInt();

            if (game == 1) {
                engine = new ArdRiEngine();
                break;
            }

            else if (game == 2) {
                engine = new BrandubhEngine();
                break;
            }

            else if (game == 3) {
                engine = new TablutEngine();
                break;
            }

            else if (game == 4) {
                System.out.print("Input field size: ");
                final int size = scanner.nextInt();

                if (size < 3) {
                    System.out.println("Size should be at least 3");
                    continue;
                }

                engine = new TicTacToeEngine(size);
                break;
            }

            else {
                System.out.println("Wrong number. Try again...");
            }
        }

        while (engine.getStatus() == Status.ACTIVE) {
            System.out.println();
            System.out.println(engine.getPresentation());
            System.out.println("Make move: ");

            try {
                if (engine instanceof final TaflEngine taflEngine) {
                    taflEngine.makeMove(new TaflMove(
                        new Vector(scanner.nextInt(), scanner.nextInt()),
                        new Vector(scanner.nextInt(), scanner.nextInt())
                    ));
                }
                else if (engine instanceof final TicTacToeEngine ticTacToeEngine) {
                    ticTacToeEngine.makeMove(new TicTacToeMove(new Vector(scanner.nextInt(), scanner.nextInt())));
                }
            } catch (InvalidMoveException exception) {
                System.out.println("Wrong move: " + exception.getMessage());
            } catch (StatusException exception) {
                System.out.println(exception.getMessage());
            }
        }

        System.out.println();
        System.out.println(engine.getPresentation());

        System.out.println(
            engine.getStatus() == Status.DRAW ?
            "Draw" :
            ("Player " + engine.getCurrentPlayer() + " won")
        );
    }
}
