package core;

public abstract class AbstractEngine<
    GameFigure extends AbstractFigure,
    GameField extends AbstractField<GameFigure>,
    GameMove extends Move
> {
    public AbstractEngine(FieldFactory<GameFigure, GameField> factory, Player initialPlayer) {
        field = factory.createField();
        currentPlayer = initialPlayer;
        status = Status.ACTIVE;
    }

    public void makeMove(final GameMove move) throws InvalidMoveException, StatusException {
        if (status != Status.ACTIVE) {
            throw new StatusException();
        }

        performMove(move);

        if (status == Status.ACTIVE) {
            currentPlayer = currentPlayer == Player.WHITE ? Player.BLACK : Player.WHITE;
        }
    }

    protected abstract void performMove(final GameMove move) throws InvalidMoveException;

    public String getPresentation() { return field.getPresentation(); }
    public Player getCurrentPlayer() { return currentPlayer; }
    public Status getStatus() { return status; }
    protected void setStatus(final Status status) { this.status = status; }

    protected final GameField field;
    private Player currentPlayer;
    private Status status;
}
