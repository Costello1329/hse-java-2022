package core;

public interface FieldFactory<GameFigure extends AbstractFigure, Field extends AbstractField<GameFigure>> {
    Field createField();
}
