package core;

public abstract class AbstractFigure {
    public AbstractFigure(Player player) { this.player = player; }
    public Player getPlayer() { return player; }
    public abstract String getPresentation();

    private final Player player;
}
