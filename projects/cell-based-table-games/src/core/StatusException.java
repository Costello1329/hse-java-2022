package core;

public class StatusException extends Exception {
    public StatusException() { super("Can't perform a move because the game is finished"); }
}
