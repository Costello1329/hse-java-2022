package tafl;

import core.Move;
import core.Vector;

public record TaflMove(Vector from, Vector to) implements Move {}
